function local_value_increase_once(adr,amount);local value = readInteger(adr);local write = writeInteger(adr,value + amount);return write;end

function local_value_decrease_once(adr,amount);local value = readInteger(adr);local write = writeInteger(adr,value - amount);return write;end

function local_value_set(adr,amount);local value = readInteger(adr);local write = writeInteger(adr,amount);return write;end

function local_float_value_increase_once(adr,amount) local value = readFloat(adr);local write = writeFloat(adr,value + amount);return write;end

function local_float_value_decrease_once(adr,amount);local value = readFloat(adr);local write = writeFloat(adr,value - amount);return write;end

function local_float_value_set(adr,amount);local value = readFloat(adr);local write = writeFloat(adr,amount);return write;end

function local_double_value_increase_once(adr,amount) local value = readDouble(adr);local write = writeDouble(adr,value + amount);return write;end

function local_double_value_decrease_once(adr,amount);local value = readDouble(adr);local write = writeDouble(adr,value - amount);return write;end

function local_double_value_set(adr,amount);local value = readDouble(adr);local write = writeDouble(adr,amount);return write;end